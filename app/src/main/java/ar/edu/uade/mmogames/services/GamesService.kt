package ar.edu.uade.mmogames.services

import android.content.Context
import android.util.Log
import ar.edu.uade.mmogames.interfaces.GamesInterface
import ar.edu.uade.mmogames.models.GameExtend
import ar.edu.uade.mmogames.models.Games
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception


class GamesService
{
    companion object
    {
        val BASE_URL = "https://www.mmobomb.com/api1/"

        fun getGames(context: Context, search: String = ""): ArrayList<Games>
        {
            Log.d("API_FETCH", "primer paso")
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            Log.d("API_FETCH", "Create retrofit api")
            val api = retrofit.create(GamesInterface::class.java)

            Log.d("API_FETCH", "execute api data")
            try
            {
                val result = api.getGames().execute()
                Log.d("API_FETCH", result.body().toString())
                return if(result.isSuccessful)
                {
                    result.body()!!
                }
                else
                {
                    ArrayList<Games>()
                }
            }
            catch(el: Exception)
            {
                Log.e("API_FETCH", el.toString())
                return ArrayList<Games>()
            }

        }

        fun getGameByID(id: String, context: Context): GameExtend
        {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            val api = retrofit.create(GamesInterface::class.java)

            try
            {
                val result = api.getGameById(id).execute()
                return if(result.isSuccessful)
                {
                    result.body()!!
                }
                else
                {
                    null!!
                }
            }
            catch(el: Exception)
            {
                return null!!
            }
        }

        fun getFavs(context: Context, db:FirebaseFirestore, auth:FirebaseAuth)
        {

        }
    }
}