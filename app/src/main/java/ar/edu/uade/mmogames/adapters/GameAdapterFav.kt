package ar.edu.uade.mmogames.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ar.edu.uade.mmogames.R
import ar.edu.uade.mmogames.models.Games
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.squareup.picasso.Picasso

// TODO - ADD Inheritance
class GameAdapterFav(private var games: ArrayList<Games>, context: Context): RecyclerView.Adapter<GameAdapterFav.GameViewHolder>()
{

    var onItemClick: ((Games) -> Unit)? = null

    class GameViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        val gameImage: ImageView = itemView.findViewById(R.id.gameItemImageF)
        val gameTitle: TextView = itemView.findViewById(R.id.gameItemTitleF)
        val gameGenre: TextView = itemView.findViewById(R.id.gameItemGenreF)
        val btnDelete: FloatingActionButton = itemView.findViewById(R.id.btnDeleteFav)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder
    {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.favourite_item, parent, false);
        return GameViewHolder(view);
    }

    override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
        val game = games[position]
        // parse image
        Picasso.get().load(game.thumbnail).into(holder.gameImage)
        holder.gameTitle.text = game.title
        holder.gameGenre.text = game.genre

        holder.itemView.setOnClickListener {
            onItemClick?.invoke(game)
        }

        holder.btnDelete.setOnClickListener {
//            onItemClick?.invoke(game) //?
            val db = FirebaseFirestore.getInstance()

            db.collection("favs")
                .document(games[position].docID!!)
                .delete()
                .addOnSuccessListener {
                    games.removeAt(position)
                    update(games)
                }
                .addOnFailureListener{
                    Log.w("DELETE_FAV", "ERROR ON DELETE FAV ${it.message }")
                }
        }
    }

    override fun getItemCount(): Int {
        return games.size
    }

    fun update(games_new: ArrayList<Games>)
    {
        games = games_new
        this.notifyDataSetChanged()
    }

}