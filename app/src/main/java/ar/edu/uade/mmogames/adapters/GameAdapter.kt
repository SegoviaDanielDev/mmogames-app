package ar.edu.uade.mmogames.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ar.edu.uade.mmogames.R
import ar.edu.uade.mmogames.models.Games
import com.squareup.picasso.Picasso

class GameAdapter(private var games: ArrayList<Games>, context: Context): RecyclerView.Adapter<GameAdapter.GameViewHolder>()
{

    var onItemClick: ((Games) -> Unit)? = null

    class GameViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        val gameImage: ImageView = itemView.findViewById(R.id.gameItemImage)
        val gameTitle: TextView = itemView.findViewById(R.id.gameItemTitle)
        val gameGenre: TextView = itemView.findViewById(R.id.gameItemGenre)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder
    {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.game_item, parent, false);
        return GameViewHolder(view);
    }

    override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
        val game = games[position]
        // parse image
        Picasso.get().load(game.thumbnail).into(holder.gameImage)
        holder.gameTitle.text = game.title
        holder.gameGenre.text = game.genre

        holder.itemView.setOnClickListener {
            onItemClick?.invoke(game)
        }
    }

    override fun getItemCount(): Int {
        return games.size
    }

    fun update(games_new: ArrayList<Games>)
    {
        games = games_new
        this.notifyDataSetChanged()
    }

}