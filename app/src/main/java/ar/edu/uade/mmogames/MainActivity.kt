package ar.edu.uade.mmogames

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ar.edu.uade.mmogames.LoginActivity.LoginActivity
import ar.edu.uade.mmogames.adapters.GameAdapter
import ar.edu.uade.mmogames.models.Games
import ar.edu.uade.mmogames.repositories.ApiRepository
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount
//import com.google.android.gms.auth.api.signin.GoogleSignInOptions
//import com.google.firebase.auth.FirebaseAuth
//import com.google.firebase.firestore.FirebaseFirestore
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity() {
    private val coroutineContext: CoroutineContext = newSingleThreadContext("apiFetch");
    private val gameUpdateContext: CoroutineContext = newSingleThreadContext("gameUpdate");
    private val scope = CoroutineScope(coroutineContext)
    private val scopeGames = CoroutineScope(gameUpdateContext)
    private val progressBar by lazy { ProgressDialog(this) }

//    private lateinit var logoutBtn: Button
    private lateinit var searchInput: EditText

    private lateinit var firebaseAuth: FirebaseAuth

    private lateinit var recyclerView: RecyclerView
    private lateinit var gameList: ArrayList<Games>
    private lateinit var gameAdapter: GameAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recyclerView = findViewById(R.id.mainRecyclerView)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)

        gameList = ArrayList()
        gameAdapter = GameAdapter(gameList, this)

        // enables to put 2 items in a row, buts its horrible

        // val gridLayout = GridLayoutManager(this, 2)
        // recyclerView.layoutManager = gridLayout

        recyclerView.adapter = gameAdapter

        gameAdapter.onItemClick = {
            val intent = Intent(this, GameDetailActivity::class.java)
            // TODO - LOADING PREVENT DOUBLE API CALL
            dispatchSelectedGame(it, intent);
        }

    }

    override fun onStart() {
        super.onStart()

        progressBar.start()

        scope.launch {
            gameList = ApiRepository.fetchData(this@MainActivity)

            val games = filterGame(searchInput.getText().toString())
            updateGameList(games)
            progressBar.stop()
        }

        firebaseAuth = FirebaseAuth.getInstance()
        checkUser()

//        logoutBtn = findViewById(R.id.button_logout)
//        logoutBtn.setOnClickListener{
//            firebaseAuth.signOut()
//            checkUser()
//        }

        searchInput = findViewById(R.id.search_games)
        searchInput.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(search: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val games = filterGame(search.toString());
                updateGameList(games)
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    private fun checkUser(){
        val firebaseUser = firebaseAuth.currentUser
        if (firebaseUser == null) {
            Log.d("LOGIN INTENT", "User not logged")
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private fun filterGame(search: String ): ArrayList<Games>
    {
        val filter: ArrayList<Games> = ArrayList()
        gameList.forEach {
            if(it.title.lowercase().contains(search!!.toString().lowercase())) {
                filter.add(it)
            }
        }

        return filter;
    }

    private fun updateGameList(games: ArrayList<Games>)
    {
        scopeGames.launch {
            withContext(Dispatchers.Main){
                gameAdapter.update(games)
            }
        }
    }

    private fun dispatchSelectedGame(it: Games, intent: Intent)
    {
        scope.launch {
            val game = ApiRepository.getGameByID(it.id.toString(), this@MainActivity)
            withContext(Dispatchers.Main) {
                intent.putExtra("game", game)
                startActivity(intent)
            }
        }
    }



}