package ar.edu.uade.mmogames

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.cardview.widget.CardView
import ar.edu.uade.mmogames.LoginActivity.LoginActivity
import ar.edu.uade.mmogames.repositories.ApiRepository
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class PrincipalMenu : AppCompatActivity() {
    private val coroutineContext: CoroutineContext = newSingleThreadContext("apiFetch");
    private val scope = CoroutineScope(coroutineContext)



    private lateinit var btnSearch: CardView;
    private lateinit var btnShuffle: CardView;
    private lateinit var btnFav: CardView;
    private lateinit var btnLogout: Button;

    private lateinit var firebaseAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal_menu)
    }

    override fun onStart() {
        super.onStart()
        btnSearch = findViewById(R.id.search_button)
        btnShuffle = findViewById(R.id.shuffle_button)
        btnFav = findViewById(R.id.fav_button)
        btnLogout = findViewById(R.id.btnLogout)

        firebaseAuth = FirebaseAuth.getInstance()

        btnFav.setOnClickListener {
            startActivity(Intent(this@PrincipalMenu, FavActivity::class.java))
        }

        btnSearch.setOnClickListener{
            startActivity(Intent(this@PrincipalMenu, MainActivity::class.java))
        }

        btnShuffle.setOnClickListener{
            scope.launch {

                try {
                    val aleatory = (0..300).random()
                    val game = ApiRepository.getGameByID(aleatory.toString(), this@PrincipalMenu)
                    withContext(Dispatchers.Main) {
                        val intent = Intent(this@PrincipalMenu, GameDetailActivity::class.java)
                        intent.putExtra("game", game)
                        startActivity(intent)
                    }
                }
                catch (e: Exception){
                    withContext(Dispatchers.Main) {
                        Toast.makeText(this@PrincipalMenu, "No se puede realizar esta accion en este momento", Toast.LENGTH_SHORT).show()
                    }
                }

            }
        }

        btnLogout.setOnClickListener {
            firebaseAuth.signOut()
            checkUser()
        }
    }

    private fun checkUser(){
        val firebaseUser = firebaseAuth.currentUser
        if (firebaseUser == null) {
            Log.d("LOGIN INTENT", "User not logged")
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

}