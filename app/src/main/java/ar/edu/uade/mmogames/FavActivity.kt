package ar.edu.uade.mmogames

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ar.edu.uade.mmogames.adapters.GameAdapter
import ar.edu.uade.mmogames.adapters.GameAdapterFav
import ar.edu.uade.mmogames.models.Games
import ar.edu.uade.mmogames.repositories.ApiRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import kotlinx.coroutines.*
import retrofit2.converter.gson.GsonConverterFactory
import kotlin.coroutines.CoroutineContext

class FavActivity : AppCompatActivity() {
    private val coroutineContext: CoroutineContext = newSingleThreadContext("apiFetch");
    private val gameUpdateContext: CoroutineContext = newSingleThreadContext("gameUpdate");
    private val scope = CoroutineScope(coroutineContext)
    private val scopeGames = CoroutineScope(gameUpdateContext)
    private val progressBar by lazy { ProgressDialog(this) }


    //    private lateinit var logoutBtn: Button
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var db: FirebaseFirestore

    private lateinit var recyclerView: RecyclerView
    private lateinit var gameList: ArrayList<Games>
    private lateinit var gameAdapter: GameAdapterFav

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fav)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        recyclerView = findViewById(R.id.favRecyclerView)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)

        gameList = ArrayList()
        gameAdapter = GameAdapterFav(gameList, this)
        gameAdapter.onItemClick = {
            val intent = Intent(this, GameDetailActivity::class.java)
            // TODO - LOADING PREVENT DOUBLE API CALL
            dispatchSelectedGame(it, intent);
        }
        // enables to put 2 items in a row, buts its horrible
        // val gridLayout = GridLayoutManager(this, 2)
        // recyclerView.layoutManager = gridLayout

        recyclerView.adapter = gameAdapter

    }

    override fun onStart() {
        super.onStart()
        progressBar.start()

        firebaseAuth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()


        scope.launch {
            gameList = ArrayList<Games>()
            db.collection("favs")
                .whereEqualTo("userId", firebaseAuth.currentUser?.uid)
                .get()
                .addOnSuccessListener {
                    for(el in it) {
                        val mapString = Gson().toJson(el.data)
                        val game = Gson().fromJson(mapString, Games::class.java)
                        game.docID = el.id
                        gameList.add(game)
                    }
                    updateGameList(gameList)
                    progressBar.stop()
                }
        }


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun updateGameList(games: ArrayList<Games>)
    {
        scopeGames.launch {
            withContext(Dispatchers.Main){
                gameAdapter.update(games)
            }
        }
    }

    private fun dispatchSelectedGame(it: Games, intent: Intent)
    {
        scope.launch {
            val game = ApiRepository.getGameByID(it.id.toString(), this@FavActivity)
            withContext(Dispatchers.Main) {
                intent.putExtra("game", game)
                startActivity(intent)
            }
        }
    }


}