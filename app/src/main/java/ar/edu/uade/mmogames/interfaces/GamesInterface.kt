package ar.edu.uade.mmogames.interfaces

import ar.edu.uade.mmogames.models.GameExtend
import ar.edu.uade.mmogames.models.Games
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GamesInterface
{
    @GET("games")
    fun getGames(): Call<ArrayList<Games>>;
    @GET("game")
    fun getGameById(@Query("id") id: String): Call<GameExtend>
}