package ar.edu.uade.mmogames.models

data class SystemRequirements (
    val os: String,
    val processor: String,
    val memory: String,
    val graphics: String,
    val storage: String
)