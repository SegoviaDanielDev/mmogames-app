package ar.edu.uade.mmogames.models

import android.os.Parcel
import android.os.Parcelable

data class Games (
    val id: Int,
    val title: String,
    val thumbnail: String,
    val short_description: String,
    val game_url: String,
    val genre: String,
    val platform: String,
    val publisher: String,
    val developer: String,
    val release_date: String,
    val profile_url: String,
    var docID: String?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(thumbnail)
        parcel.writeString(short_description)
        parcel.writeString(game_url)
        parcel.writeString(genre)
        parcel.writeString(platform)
        parcel.writeString(publisher)
        parcel.writeString(developer)
        parcel.writeString(release_date)
        parcel.writeString(profile_url)
        parcel.writeString(docID)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Games> {
        override fun createFromParcel(parcel: Parcel): Games {
            return Games(parcel)
        }

        override fun newArray(size: Int): Array<Games?> {
            return arrayOfNulls(size)
        }
    }


}