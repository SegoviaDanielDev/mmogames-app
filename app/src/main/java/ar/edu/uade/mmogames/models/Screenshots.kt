package ar.edu.uade.mmogames.models

data class Screenshots (
    val id: Int,
    val image: String
)