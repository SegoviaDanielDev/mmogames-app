package ar.edu.uade.mmogames.repositories

import android.content.Context
import ar.edu.uade.mmogames.models.GameExtend
import ar.edu.uade.mmogames.models.Games
import ar.edu.uade.mmogames.services.GamesService
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class ApiRepository
{
    companion object
    {


        suspend fun fetchData(context: Context): ArrayList<Games>
        {
            return GamesService.getGames(context)
        }

        suspend fun getGameByID(id: String, context: Context): GameExtend
        {
            return GamesService.getGameByID(id, context)
        }

        suspend fun getFirebaseFavs(context: Context, db:FirebaseFirestore, auth: FirebaseAuth)
        {
            return GamesService.getFavs(context, db, auth);
        }
    }
}