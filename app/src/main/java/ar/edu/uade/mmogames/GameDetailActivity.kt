package ar.edu.uade.mmogames

import android.content.res.ColorStateList
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import ar.edu.uade.mmogames.models.GameExtend
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso


class GameDetailActivity : AppCompatActivity() {

    private lateinit var btnFavInfo: FloatingActionButton
    private lateinit var db: FirebaseFirestore
    private lateinit var firebaseAuth: FirebaseAuth

    private var game: GameExtend? = null
    var id: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        game = intent.getParcelableExtra("game")
        if(game != null)
        {
            val gameTitle: TextView = findViewById(R.id.gameDetailTitle)
            val gameGenre: TextView = findViewById(R.id.gameDetailGenre)
            val gameDescription: TextView = findViewById(R.id.gameDetailDescription)
            val gameImage: ImageView = findViewById(R.id.gameDetailImage)

            gameTitle.text = game?.title
            gameGenre.text = game?.genre
            gameDescription.text = game?.description?.replace(Regex("<.*?>"), "");
            Picasso.get().load(game?.thumbnail).into(gameImage)

        }

    }

    override fun onStart() {
        super.onStart()

        btnFavInfo = findViewById(R.id.btnFavInfo)
        btnFavInfo.setOnClickListener {
            if(id != null) deleteFromFavs(id!!) else addToFavs()
        }

        firebaseAuth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()

        db.collection("favs")
            .whereEqualTo("userId", firebaseAuth.currentUser!!.uid)
            .get()
            .addOnSuccessListener {

                for(el in it) {
                    if(el.data["id"].toString() == game?.id.toString()) {
                        btnFavInfo.setBackgroundColor( resources.getColor(R.color.mmoRed))
                        btnFavInfo.setImageResource(R.drawable.delete_svgrepo_com)
                        id = el.id
                        break
                    }
                }

            }
            .addOnFailureListener{
                Log.w("FAV_BUTTON_TRIGGER", "error: "+ it.message.toString())
            }


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun deleteFromFavs(idDelete: String)
    {
        db.collection("favs").document(idDelete)
            .delete()
            .addOnSuccessListener {
                Log.d("DELETE_COLLECTION", "SUCCESS DELETE ACTION $idDelete")
                btnFavInfo.setImageResource(R.drawable.estrella)
                btnFavInfo.setBackgroundColor( resources.getColor(R.color.warning))
                id = null
                Toast.makeText(this, "Juego eliminado de favoritos correctamente", Toast.LENGTH_SHORT).show()

            }
            .addOnFailureListener {
                Log.d("DELETE_COLLECTION", "ERROR DELETE ACTION ${it.message}")
            }
    }

    fun addToFavs()
    {
        val gameFirestore = hashMapOf(
            "id" to game?.id,
            "title" to game?.title,
            "thumbnail" to game?.thumbnail,
            "status" to game?.status,
            "short_description" to game?.short_description,
            "description" to game?.description,
            "game_url" to game?.game_url,
            "genre" to game?.genre,
            "platform" to game?.platform,
            "publisher" to game?.publisher,
            "developer" to game?.developer,
            "release_date" to game?.release_date,
            "profile_url" to game?.profile_url,
            "userId" to firebaseAuth.currentUser?.uid
        )

        db.collection("favs")
            .add(gameFirestore)
            .addOnSuccessListener {
                Log.d("ADD_COLLECTION", "SUCCESS ADD ACTION ${it.id}")
                btnFavInfo.setImageResource(R.drawable.delete_svgrepo_com)
                btnFavInfo.setBackgroundColor( resources.getColor(R.color.mmoRed))
                id = it.id
                Toast.makeText(this, "Juego agregado a favoritos correctamente", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener {
                Log.d("ADD_COLLECTION", "ERROR ADD ACTION ${it.message}")
            }
    }

}