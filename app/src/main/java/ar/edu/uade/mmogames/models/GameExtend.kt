package ar.edu.uade.mmogames.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class GameExtend (
    val id: Int,
    val title: String,
    val thumbnail: String,
    val status: String,
    val short_description: String,
    val description: String,
    val game_url: String,
    val genre: String,
    val platform: String,
    val publisher: String,
    val developer: String,
    val release_date: String,
    val profile_url: String,

    //TODO - SAVE THIS DATA WITH PARCELABLE

//    @SerializedName("minimun_system_requirements")
//    val systemRequirements: SystemRequirements,
//    val screenshots: ArrayList<Screenshots>

): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(thumbnail)
        parcel.writeString(status)
        parcel.writeString(short_description)
        parcel.writeString(description)
        parcel.writeString(game_url)
        parcel.writeString(genre)
        parcel.writeString(platform)
        parcel.writeString(publisher)
        parcel.writeString(developer)
        parcel.writeString(release_date)
        parcel.writeString(profile_url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GameExtend> {
        override fun createFromParcel(parcel: Parcel): GameExtend {
            return GameExtend(parcel)
        }

        override fun newArray(size: Int): Array<GameExtend?> {
            return arrayOfNulls(size)
        }
    }
}